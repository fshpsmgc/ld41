﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {
    public bool EnableSpawn;
    public GameObject[] Enemy;
    public float SpawnRate;
    float TimePassed;

	// Use this for initialization
	void Start () {
        Spawn();
	}
	
	// Update is called once per frame
	void Update () {
        if (EnableSpawn)
        {
            TimePassed += Time.deltaTime;

            if (TimePassed >= SpawnRate)
            {
                Spawn();
                TimePassed = 0;
            }
        }
	}

    public void Spawn()
    {
        Instantiate(Enemy[Random.Range(0, Enemy.Length)], transform.position, transform.rotation);
    }
}
