﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Customer : MonoBehaviour {
    public string[] Ingredients;
    public int Points;
    public float WaitTime;
    float TimePassed;

    Slider TimeSlider;

    AudioSource OrderSuccess;
    AudioSource OrderFail;
	// Use this for initialization
	void Start () {
        TimeSlider = GameObject.Find("WaitTimerSlider").GetComponent<Slider>();
        TimeSlider.maxValue = WaitTime;
        TimeSlider.value = WaitTime;

        OrderSuccess = GameObject.Find("Order_Success").GetComponent<AudioSource>();
        OrderFail = GameObject.Find("Order_Failure").GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        TimePassed += Time.deltaTime;
        TimeSlider.value = WaitTime - TimePassed;
        if(TimePassed >= WaitTime)
        {
            Fail();
        }
	}

    void OnTriggerStay2D(Collider2D other)
    {
        if(other.tag == "Plate" && !other.GetComponent<Plate>().Attached)
        {
            bool successFlag = true;

            if(other.transform.childCount != Ingredients.Length)
            {
                Fail();
            }else
            {

                for (int i = 0; i < other.transform.childCount; i++)
                {
                    print("Iteration: " + i + ";" + other.transform.GetChild(i).name + ";" + "SuccessFlag: " + successFlag.ToString());
                    if (successFlag == true)
                    {
                        successFlag = false;
                        for (int j = 0; j < Ingredients.Length; j++)
                        {
                            if (other.transform.GetChild(i).name.Contains(Ingredients[j]))
                            {
                                print("\tHit");
                                successFlag = true;
                            }else
                            {
                                print("\tMiss");
                            }
                        }
                    }
                    else
                    {
                        break;
                    }

                }

                if (successFlag)
                {
                    Success();
                }
                else
                {
                    Fail();
                }
            }

            Destroy(other.gameObject);
        }
    }

    void Success()
    {
        GameObject.Find("CustomersController").GetComponent<CustomersController>().GenerateCustomer();
        GameObject.Find("PlayerController").GetComponent<PlayerController>().AddPoints(Points);
        print("Yay");
        OrderSuccess.Play();
        Destroy(gameObject);
    }

    void Fail()
    {
        GameObject.Find("PlayerController").GetComponent<PlayerController>().DetractLives();
        if(GameObject.Find("PlayerController").GetComponent<PlayerController>().Lives > 0)
        {
            GameObject.Find("CustomersController").GetComponent<CustomersController>().GenerateCustomer();
        }       
        print("Boo");
        OrderFail.Play();
        Destroy(gameObject);
    }
}
