﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slice : MonoBehaviour {
    public bool Attached;
    public bool CanBeAttached;
    Rigidbody2D body;

    AudioSource PickupSound;
    AudioSource ReleaseSound;

	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
        PickupSound = GameObject.Find("Slice_Pickup").GetComponent<AudioSource>();
        ReleaseSound = GameObject.Find("Slice_Release").GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonUp(0) && Attached)
        {
            Attached = false;
            ReleaseSound.Play();
            GetComponent<Collider2D>().enabled = true;
        }

        if (Attached)
        {
            body.MovePosition(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 8)));
            //transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 8));
        }
	}

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (CanBeAttached)
            {
                Attached = true;
                PickupSound.Play();
                GetComponent<Collider2D>().enabled = false;
            }

        }
    }
}
