﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plate : MonoBehaviour {
    public bool Attached;
    Rigidbody2D body;

    AudioSource PickupSound;
    AudioSource ReleaseSound;

    AudioSource SlicePickup;
    // Use this for initialization
    void Start()
    {
        Attached = true;
        body = GetComponent<Rigidbody2D>();

        PickupSound = GameObject.Find("Plate_Pickup2").GetComponent<AudioSource>();
        ReleaseSound = GameObject.Find("Plate_Release").GetComponent<AudioSource>();
        SlicePickup = GameObject.Find("Slice_Release").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0) && Attached)
        {
            Attached = false;
            ReleaseSound.Play();
        }

        if (Attached)
        {
            body.MovePosition(Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 8.5f)));
            //transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 8.5f));
        }
    }

    void OnTriggerStay2D(Collider2D col)
    {
        if (col.tag == "Slice" && !col.GetComponent<Slice>().Attached)
        {
            col.transform.parent = transform;
            //col.transform.Translate(new Vector3(0, 0, -1));
            col.GetComponent<Slice>().CanBeAttached = false;
            col.GetComponent<Rigidbody2D>().simulated = false;

            if(Attached)
                SlicePickup.Play();
            
        }
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Attached = true;
            PickupSound.Play();
        }
    }
}
