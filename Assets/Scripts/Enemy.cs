﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {
    public int Health;
    public float Speed;
    public GameObject Slice;
    PlayerController Player;
    Rigidbody2D body;
	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        body.velocity = Vector2.right * Speed;
	}

    void OnMouseOver()
    {
        if(Player == null)
        {
            Player = GameObject.Find("PlayerController").GetComponent<PlayerController>();
        }

        Player.isAllowedToShoot = true;
        Cursor.SetCursor(Player.Crosshair_Targeted, Vector2.zero, CursorMode.Auto);

        if (Input.GetMouseButtonDown(0))
        {
            Damage();
        }
    }

    void OnMouseExit()
    {
        Cursor.SetCursor(Player.Crosshair, Vector2.zero, CursorMode.Auto);
        Player.isAllowedToShoot = false;
    }

    void Damage()
    {
        Health--;
        GetComponent<SpriteRenderer>().color = new Color32((byte)((255/5)*Health), (byte)((255 / 5) * Health), (byte)((255 / 5) * Health), 255);
        if (Health <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        Player.isAllowedToShoot = false;
        Instantiate(Slice, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
