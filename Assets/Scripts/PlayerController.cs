﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {
    public Texture2D Crosshair;
    public Texture2D Crosshair_Targeted;

    public enum GameMode
    {
        Cooking,
        Shooting
    }

    public Transform CameraPosition1;
    public Transform CameraPosition2;
    Transform Current_CameraPosition;
    public float CameraTransitionSpeed;

    public Spawner Spawn;

    public int Points;
    public int Lives;

    public Canvas StartupScreen;
    public Canvas GameOverUI;
    public Canvas GameUI;
    public Text ScoreText;
    public string[] AdditionalScoreText;
    public Image[] HeartContainers;

    public AudioSource[] ShootingSounds;

    public bool isAllowedToShoot;
	// Use this for initialization
	void Start () {
        Current_CameraPosition = CameraPosition1;

        StartupScreen.enabled = true;
        GameUI.enabled = false;
        GameOverUI.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        Camera.main.transform.position = new Vector3(Mathf.Lerp(Camera.main.transform.position.x, Current_CameraPosition.transform.position.x, CameraTransitionSpeed), Mathf.Lerp(Camera.main.transform.position.y, Current_CameraPosition.transform.position.y, CameraTransitionSpeed), Camera.main.transform.position.z);

        if(Current_CameraPosition == CameraPosition1)
        {
            if (Input.GetKeyDown(KeyCode.D))
            {
                SetToShoot();
            }
        }else if(Current_CameraPosition == CameraPosition2)
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                SetToCook();
            }

            if (Input.GetMouseButtonDown(0) && isAllowedToShoot)
            {
                ShootingSounds[Random.Range(0, ShootingSounds.Length)].Play();
            }
        }
    }

    void SetToCook()
    {
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
        Current_CameraPosition = CameraPosition1;
        Spawn.EnableSpawn = true;
    }

    void SetToShoot()
    {
        Cursor.SetCursor(Crosshair, Vector2.zero, CursorMode.Auto);
        Current_CameraPosition = CameraPosition2;
        Spawn.EnableSpawn = true;
    }

    public void AddPoints(int points)
    {
        Points += points;
    }

    public void DetractLives()
    {
        print("haha");
        Lives--;
        HeartContainers[Lives].enabled = false;
        if(Lives <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        print("Death");
        GameOverUI.enabled = true;
        GameUI.enabled = false;
        ScoreText.text = AdditionalScoreText[Random.Range(0, AdditionalScoreText.Length)] + Points;
        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }

    public void Restart()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }

    public void HideTutorial()
    {
        StartupScreen.enabled = false;
        GameUI.enabled = true;
    }
}
