﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateDispencer : MonoBehaviour {
    public GameObject Plates;
    GameObject prevPlate;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if(prevPlate != null)
            {
                Destroy(prevPlate.gameObject);
            }
            GameObject.Find("Plate_Pickup2").GetComponent<AudioSource>().Play();
            prevPlate = Instantiate(Plates, transform.position, transform.rotation);
        }
    }
}
