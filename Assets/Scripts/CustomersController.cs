﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomersController : MonoBehaviour {
    public Transform CustomerOrigin;
    public GameObject[] CustomerPool;

    public float BaseWaitTime;
    public float MinTime;
    [Range(0,1)]
    public float TimeRemainingPercentage;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void GenerateCustomer()
    {
        GameObject tmp = Instantiate(CustomerPool[Random.Range(0, CustomerPool.Length)], CustomerOrigin.position, CustomerOrigin.rotation);
        BaseWaitTime *= TimeRemainingPercentage;
        if(BaseWaitTime < MinTime)
        {
            BaseWaitTime = MinTime;
        }
        tmp.GetComponent<Customer>().WaitTime = BaseWaitTime;
    }
}
